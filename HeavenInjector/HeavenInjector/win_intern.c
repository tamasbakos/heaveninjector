
#include "win_intern.h"

#include <Psapi.h>
#include <stdio.h>
#include "macro_overload.h"

#define PRINTERRORLINE(string) printf(\
                "[FAIL] %s  - Line: %d Errorcode: <0x%X>        \n",\
                (string),\
                __LINE__,\
                GetLastError())


#ifndef _WIN64

#define EMIT(byte_val) __asm __emit (byte_val)

#define DB1(b)      EMIT(b)
#define DB2(b, ...) EMIT(b) EXPAND( DB1(__VA_ARGS__))
#define DB3(b, ...) EMIT(b) EXPAND( DB2(__VA_ARGS__))
#define DB4(b, ...) EMIT(b) EXPAND( DB3(__VA_ARGS__))
#define DB5(b, ...) EMIT(b) EXPAND( DB4(__VA_ARGS__))
#define DB6(b, ...) EMIT(b) EXPAND( DB5(__VA_ARGS__))
#define DB7(b, ...) EMIT(b) EXPAND( DB6(__VA_ARGS__))
#define DB8(b, ...) EMIT(b) EXPAND( DB7(__VA_ARGS__))
#define DB9(b, ...) EMIT(b) EXPAND( DB8(__VA_ARGS__))
#define DB10(b, ...) EMIT(b) EXPAND( DB9(__VA_ARGS__))
#define DB11(b, ...) EMIT(b) EXPAND( DB10(__VA_ARGS__))
#define DB12(b, ...) EMIT(b) EXPAND( DB11(__VA_ARGS__))
#define DB13(b, ...) EMIT(b) EXPAND( DB12(__VA_ARGS__))
#define DB14(b, ...) EMIT(b) EXPAND( DB13(__VA_ARGS__))
#define DB15(b, ...) EMIT(b) EXPAND( DB14(__VA_ARGS__))
#define DB16(b, ...) EMIT(b) EXPAND( DB15(__VA_ARGS__))
#define DB17(b, ...) EMIT(b) EXPAND( DB16(__VA_ARGS__))
#define DB18(b, ...) EMIT(b) EXPAND( DB17(__VA_ARGS__))
#define DB19(b, ...) EMIT(b) EXPAND( DB18(__VA_ARGS__))
#define DB20(b, ...) EMIT(b) EXPAND( DB19(__VA_ARGS__))

#define DB(...) OVERLOAD(DB, __VA_ARGS__)


#define SwitchToX64() \
{ \
    DWORD dwEspBackup = 0;                                     \
    __asm mov dwEspBackup, esp   /* back up stack pointer  */  \
    __asm and sp, 0xFFF8         /* align stack to 8 bytes */  \
    DB(0x6A, 0x33)                      /*  push   0x33                  */ \
    DB(0xE8, 0x00, 0x00, 0x00, 0x00)    /*  call   $+5                   */ \
    DB(0x83, 0x04, 0x24, 0x05)          /*  add    dword [esp], 5        */ \
    DB(0xCB)                            /*  retf                         */

#define SwitchBackToX86() \
 \
    DB(0xE8, 0x00, 0x00, 0x00, 0x00)                    /*  call   $+5                   */ \
    DB(0xC7, 0x44, 0x24, 0x04, 0x23, 0x00, 0x00, 0x00)  /*  mov    dword [rsp + 4], 0x23 */ \
    DB(0x83, 0x04, 0x24, 0x0D)                          /*  add    dword [rsp], 13       */ \
    DB(0xCB)                                            /*  retf                         */ \
    __asm mov    esp, dwEspBackup  /* restore original stack pointer */         \
}

#define X86PushA() \
{ \
    DB(0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57)   \
    /*  eax,  ecx,  edx,  ebx,  esp,  ebp,  esi,  edi */ \
}

#define X86PopA() \
{ \
    DB(0x5F, 0x5E, 0x5D, 0x5C, 0x5B, 0x5A, 0x59, 0x58)   \
    /*  edi,  esi,  ebp,  esp,  ebx,  edx,  ecx,  eax */ \
}

#define X64PushA() \
{ \
    DB(0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57)   \
    /*  rax,  rcx,  rdx,  rbx,  rsp,  rbp,  rsi,  rdi */ \
                                                             \
    DB(0x41, 0x50, 0x41, 0x51, 0x41, 0x52, 0x41, 0x53)       \
    /*         r8,         r9,        r10,        r11 */     \
                                                             \
    DB(0x41, 0x54, 0x41, 0x55, 0x41, 0x56, 0x41, 0x57)       \
    /*        r12,        r13,        r14,        r15 */     \
}

#define X64PopA() \
{ \
    DB(0x41, 0x5F, 0x41, 0x5E, 0x41, 0x5D, 0x41, 0x5C)       \
    /*        r15,        r14,        r13,        r12 */     \
                                                             \
    DB(0x41, 0x5B, 0x41, 0x5A, 0x41, 0x59, 0x41, 0x58)       \
    /*        r11,        r10,         r9,         r8 */     \
                                                             \
    DB(0x5F, 0x5E, 0x5D, 0x5C, 0x5B, 0x5A, 0x59, 0x58)   \
    /*  rdi,  rsi,  rbp,  rsp,  rbx,  rdx,  rcx,  rax */ \
}


#endif // !_WIN64

HMODULE
WINAPI
GetRemoteModuleHandleA(
    _In_opt_ HANDLE hProcess,
    _In_opt_ LPCSTR lpModuleName,
    _In_  DWORD dwBitnessFilterFlag
)
{
    HMODULE retHModule = NULL;
    BOOL apiStatus = FALSE;

    HANDLE hHeap = NULL;

    LPWSTR wideName = NULL;
    DWORD nrChars = 0;

    __try
    {
        hHeap = GetProcessHeap();

        if (NULL == hHeap)
        {
            __leave;
        }

        if (NULL != lpModuleName)
        {
            nrChars = (DWORD)strlen(lpModuleName) + 1;

            wideName = (WCHAR*)HeapAlloc(
                hHeap,
                HEAP_ZERO_MEMORY,
                nrChars * sizeof(WCHAR)
            );

            if (NULL == wideName)
            {
                __leave;
            }

            apiStatus = MultiByteToWideChar(
                CP_ACP,
                0,
                lpModuleName,
                -1,
                wideName,
                MAX_PATH
            );

            if (!apiStatus)
            {
                __leave;
            }
        }

        retHModule = GetRemoteModuleHandleW(
            hProcess,
            wideName,
            dwBitnessFilterFlag
        );
    }
    __finally
    {
        if (NULL != wideName)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                wideName
            );

            if (!apiStatus)
            {
                ;
            }

            wideName = NULL;
        }
    }

    return retHModule;
}

HMODULE
WINAPI
GetRemoteModuleHandleW(
    _In_opt_ HANDLE hProcess,
    _In_opt_ LPCWSTR lpModuleName,
    _In_  DWORD dwBitnessFilterFlag
)
{
    HMODULE retHModule = NULL;
    BOOL apiStatus = FALSE;
    //    NTSTATUS ntStatus = 0;
    DWORD i = 0;

    HANDLE hHeap = NULL;

    //    HMODULE hNtDll = NULL;

    //    pNtQueryInformationProcess NtQueryInformationProcess = NULL;

    HMODULE* hModuleArray = NULL;
    DWORD dwNeededBytes = 0;
    DWORD dwNrModules = 0;

    WCHAR currentModuleName[MAX_PATH] = { 0 };
    WCHAR lpModuleNameCopy[MAX_PATH] = { 0 };

    //    PEB_LDR_DATA32 peb_ldr32 = { 0 };
    //    PEB_LDR_DATA64 peb_ldr64 = { 0 };

    //    LIST_ENTRY32* moduleListHead32 = NULL;
    //    LIST_ENTRY64* moduleListHead64 = NULL;

    //    LIST_ENTRY32* moduleListPtr32 = NULL;
    //    LIST_ENTRY64* moduleListPtr64 = NULL;

    //    LDR_MODULE32 current_module32 = { 0 };
    //    LDR_MODULE64 current_module64 = { 0 };

    //    UNICODE_STRING32* moduleName32 = NULL;
    //    UNICODE_STRING64* moduleName64 = NULL;

    //    PROCESS_BASIC_INFORMATION procBasicInfo = { 0 };
    //    BOOL wow64Proc = FALSE;
    //    BOOL win64bit = FALSE;

    // GetModuleHandleW;

    __try
    {
        if (NULL == hProcess)
        {
            hProcess = GetCurrentProcess();
        }

        hHeap = GetProcessHeap();

        if (NULL == hHeap)
        {
            __leave;
        }

        /*
        hNtDll = LoadLibraryW(L"ntdll.dll");

        if (NULL == hNtDll)
        {
            __leave;
        }

        NtQueryInformationProcess =
            (pNtQueryInformationProcess)GetProcAddress(
                hNtDll,
                "NtQueryInformationProcess");

        if (NULL == NtQueryInformationProcess)
        {
            __leave;
        }

        ntStatus = NtQueryInformationProcess(
            hProcess,
            ProcessBasicInformation,
            &procBasicInfo,
            sizeof(PROCESS_BASIC_INFORMATION),
            NULL
        );

        if (ntStatus != 0)
        {
            __leave;
        }
        */
        /*
        apiStatus = IsWow64Process(
            hProcess,
            &wow64Proc
        );

        if (!apiStatus)
        {
            __leave;
        }

        apiStatus = IsWindows64Bit(
            &win64bit
        );

        if (!apiStatus)
        {
            __leave;
        }

        // read the PEB of the process
        // check which PEB to use

        // if process is 32bits
        if (wow64Proc || !win64bit)
        {
            apiStatus = ReadProcessMemory(
                hProcess,
                (LPCVOID)(ULONG_PTR)(((PEB32*)procBasicInfo.PebBaseAddress)->Ldr),
                &peb_ldr32,
                sizeof(PEB_LDR_DATA32),
                NULL
            );

            if (!apiStatus)
            {
                __leave;
            }

            moduleListHead32 = &(peb_ldr32.InLoadOrderModuleList);
            moduleListPtr32 = moduleListHead32;

            do
            {
                moduleListPtr32 = moduleListPtr32->Flink;
                apiStatus = ReadProcessMemory(
                    hProcess,
                    (LPCVOID)(LDR_MODULE32*)moduleListPtr32,
                    &current_module32,
                    sizeof(LDR_MODULE32),
                    NULL
                );

                if (!apiStatus)
                {
                    __leave;
                }



                if (0 == wcsstr(moduleName32->Buffer, lpModuleNameCopy))
                {

                }

            } while (moduleListPtr32->Flink != moduleListHead32);

        }
        else // if (!wow64Proc && win64bit)
        {
            apiStatus = ReadProcessMemory(
                hProcess,
                (LPCVOID)(ULONG_PTR)(((PEB64*)procBasicInfo.PebBaseAddress)->Ldr),
                &peb_ldr64,
                sizeof(PEB_LDR_DATA64),
                NULL
            );

            if (!apiStatus)
            {
                __leave;
            }

            moduleListHead64 = &(peb_ldr64.InLoadOrderModuleList);
            moduleListPtr64 = moduleListHead64;

            do
            {


            } while (moduleListPtr64->Flink != moduleListHead64);

        }

        */

        // find out how many bytes are needed
        // to enumerate all the modules of the process
        apiStatus = EnumProcessModulesEx(
            hProcess,
            hModuleArray,
            0,
            &dwNeededBytes,
            dwBitnessFilterFlag
        );

        if (!apiStatus)
        {
            __leave;
        }

        // calculate the number of modules
        dwNrModules = dwNeededBytes / sizeof(HMODULE);

        // allocate space for the array of module handles
        hModuleArray = (HMODULE*)HeapAlloc(
            hHeap,
            HEAP_ZERO_MEMORY,
            dwNrModules * sizeof(HMODULE)
        );

        if (NULL == hModuleArray)
        {
            __leave;
        }

        // enumerate all modules of the process

        apiStatus = EnumProcessModulesEx(
            hProcess,
            hModuleArray,
            dwNrModules * sizeof(HMODULE),
            &dwNeededBytes,
            dwBitnessFilterFlag
        );

        if (!apiStatus)
        {
            __leave;
        }

        if (NULL == lpModuleName)
        {
            retHModule = hModuleArray[0];
            __leave;
        }

        // make a lowercase copy of the module name, 
        // so that it will be easier to compare
        apiStatus = wcscpy_s(
            lpModuleNameCopy,
            MAX_PATH,
            lpModuleName
        );

        if (0 != apiStatus)
        {
            __leave;
        }

        apiStatus = _wcslwr_s(lpModuleNameCopy, MAX_PATH);

        if (0 != apiStatus)
        {
            __leave;
        }


        // check each module's name, 
        // if it corresponds to the name of the needed module
        for (i = 0; i < dwNrModules; i++)
        {
            // get the name of a module
            apiStatus = GetModuleBaseNameW(
                hProcess,
                hModuleArray[i],
                currentModuleName,
                MAX_PATH
            );

            if (!apiStatus)
            {
                __leave;
            }

            // make module name lowercase
            apiStatus = _wcslwr_s(currentModuleName, MAX_PATH);

            if (0 != apiStatus)
            {
                __leave;
            }


            // check if found modulename corresponds
            // to the name of the needed module
            if (NULL != wcsstr(currentModuleName, lpModuleNameCopy))
            {
                retHModule = hModuleArray[i];
                break;
            }
        }

    }
    __finally
    {
        if (NULL != hModuleArray)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                hModuleArray
            );

            if (!apiStatus)
            {
                ;
            }

            hModuleArray = NULL;
        }
    }

    return retHModule;
}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

#ifndef _WIN64

// This only works when called from a 32bit process on a 64bit system

QWORD
WINAPI
Wow64GetNativeModuleHandleA(
    _In_opt_ LPCSTR lpModuleName
)
{

    QWORD retHModule = 0;
    BOOL apiStatus = FALSE;

    HANDLE hHeap = NULL;

    LPWSTR wName = NULL;
    DWORD dwNrChars = 0;

    __try
    {
        hHeap = GetProcessHeap();

        if (NULL == hHeap)
        {
            __leave;
        }

        if (NULL != lpModuleName)
        {
            dwNrChars = (DWORD)strlen(lpModuleName) + 1;

            wName = (WCHAR*)HeapAlloc(
                hHeap,
                HEAP_ZERO_MEMORY,
                dwNrChars * sizeof(WCHAR)
            );

            if (NULL == wName)
            {
                __leave;
            }

            apiStatus = MultiByteToWideChar(
                CP_ACP,
                0,
                lpModuleName,
                -1,
                wName,
                MAX_PATH
            );

            if (!apiStatus)
            {
                __leave;
            }
        }

        retHModule = Wow64GetNativeModuleHandleW(wName);
    }
    __finally
    {
        if (NULL != wName)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                wName
            );

            if (!apiStatus)
            {
                ;
            }

            wName = NULL;
        }
    }

    return retHModule;
}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


QWORD
WINAPI
Wow64GetNativeModuleHandleW(
    _In_opt_ LPCWSTR lpModuleName
)
{
    __debugbreak();

    QWORD retHModule = 0;
    BOOL apiStatus = FALSE;
    BOOL wow64Proc = FALSE;

    ULARGE_INTEGER qw = { 0 };

    __try
    {
        apiStatus = IsWow64Process(
            GetCurrentProcess(),
            &wow64Proc
        );

        if (!apiStatus)
        {
            __leave;
        }

        if (!wow64Proc) // if not wow64 process
        {
            SetLastError(ERROR_INVALID_FUNCTION);
            __leave;
        }

        if (NULL == lpModuleName)
        {
            qw.LowPart = (DWORD)GetModuleHandleW(NULL);
            retHModule = qw.QuadPart;
            __leave;
        }

        SwitchToX64();
        X64PushA();

        // rax = &TEB
        // mov rax, gs:[0x30]
        DB(0x65, 0x48, 0x8B, 0x04, 0x25, 0x30, 0x00, 0x00, 0x00);

        // rax = &PEB
        // mov rax, [rax + 0x60]
        DB(0x48, 0x8B, 0x40, 0x60);

        // rax = &PEB_LDR_DATA (PEB.Ldr)
        // mov rax, [rax + 0x18]
        DB(0x48, 0x8B, 0x40, 0x18);

        // rbx = &PEB_LDR_DATA->InInitializationOrderModuleList (pointer to list head)
        // lea rbx, [rax + 0x30]
        DB(0x48, 0x8D, 0x58, 0x30);

        // rax = PEB_LDR_DATA.InInitOrderModuleList.Flink (pointer to first LDR_MODULE)
        // mov rax, [rax + 0x30]
        DB(0x48, 0x8B, 0x40, 0x30);



        // check_next_module:

        // -- push 0
        // xor rcx, rcx
        DB(0x48, 0x31, 0xC9);
        // push rcx
        DB(0x51);

        // if rax (Flink) == rbx (ListHead)
        // cmp rax, rbx
        DB(0x48, 0x39, 0xD8);
        // je finished_search
        DB(0x74, 0x28);

        // add rsp, 8
        DB(0x48, 0x83, 0xC4, 0x08);


        // rax = &LDR_DATA_TABLE_ENTRY ( &LDR_MODULE )
        // sub rax, 0x20
        DB(0x48, 0x83, 0xE8, 0x20);


        // rsi = LDR_DATA_TABLE_ENTRY.BaseDllName.Buffer
        // mov rsi, [rax + 0x60]
        DB(0x48, 0x8B, 0x70, 0x60);

        // rdi = parameter Dll name
        // xor rdi, rdi
        DB(0x48, 0x31, 0xFF);
        // mov edi, [rbp + 0x8] -- ret adr + ebp
        DB(0x8B, 0x7D, 0x08);

        // rcx = LDR_DATA_TABLE_ENTRY.BaseDllName.Length
        // xor rcx, rcx
        DB(0x48, 0x31, 0xC9);
        // mov cx, [rax + 0x5A]
        DB(0x66, 0x8B, 0x48, 0x5A);

        // compare LDR_DATA_TABLE_ENTRY.BaseDllName.Buffer with parameter
        // repe cmpsb
        DB(0xF3, 0xA6);

        // if equal, jump to found_module
        // je found_module
        DB(0x74, 0x06); // 2 bytes

        // rax = rax->InInitializationOrderModuleList.Flink
        // mov rax, [rax + 0x20]
        DB(0x48, 0x8B, 0x40, 0x20);


        // jmp check_next_module
        DB(0xEB, 0xD4); // 2 bytes



        // found_module:

        // rax = DllBase address
        // mov rax, [rax + 0x30]
        DB(0x48, 0x8B, 0x40, 0x30);

        // push rax
        DB(0x50);

        // finished_search:
        __asm pop qw.LowPart

        X64PopA();
        SwitchBackToX86();

        retHModule = qw.QuadPart;
    }
    __finally
    {
    }

    return retHModule;
}

QWORD
Wow64GetNativeProcAddress(
    _In_ QWORD   hModule,
    _In_ LPCSTR  lpProcName
)
{
    __debugbreak();

    QWORD retProcAddr = 0;
    BOOL apiStatus = FALSE;
    BOOL wow64Proc = FALSE;
    QWORD qwLpProcName = (QWORD)lpProcName;

    ULARGE_INTEGER qw = { 0 };
    __try
    {

        apiStatus = IsWow64Process(
            GetCurrentProcess(),
            &wow64Proc
        );

        if (!apiStatus)
        {
            __leave;
        }

        if (!wow64Proc) // if not wow64 process
        {
            SetLastError(ERROR_INVALID_FUNCTION);
            __leave;
        }

        SwitchToX64();
        X64PushA();

#pragma warning(push)
#pragma warning(disable: 4409) // illegal instruction size
        __asm push hModule
        __asm push qwLpProcName
#pragma warning(pop)
        
        // r10 = lpProcName
        // pop r10
        DB(0x41, 0x5A);

        // rbx = &IMAGE_DOS_HEADER (image base)
        // pop rbx
        DB(0x5B);

        // rax = IMAGE_DOS_HEADER.e_lfanew
        // xor rax, rax
        DB(0x48, 0x31, 0xC0);
        // mov eax, [rbx + 0x3c]
        DB(0x8B, 0x43, 0x3C);

        // rax = &IMAGE_NT_HEADERS
        // add rax, rbx
        DB(0x48, 0x01, 0xD8);

        // rax = &IMAGE_FILE_HEADER
        // add rax, 4
        DB(0x48, 0x83, 0xC0, 0x04);

        // rax = &IMAGE_OPTIONAL_HEADER64
        // add rax, 20
        DB(0x48, 0x83, 0xC0, 0x14);

        // rax = &IMAGE_DATA_DIRECTORY(_array)[IMAGE_DIRECTORY_ENTRY_EXPORT]
        // add rax, 0x70
        DB(0x48, 0x83, 0xC0, 0x70);

        // rax = IMAGE_DATA_DIRECTORY[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress
        // mov eax, [rax]
        DB(0x8B, 0x00);

        // clear upper 32bits of rax
        // shl rax, 32
        DB(0x48, 0xC1, 0xE0, 0x20);
        // shr rax, 32
        DB(0x48, 0xC1, 0xE8, 0x20);

        // rax = &IMAGE_EXPORT_DIRECTORY
        // add rax, rbx
        DB(0x48, 0x01, 0xD8);

        // r9 = IMAGE_EXPORT_DIRECTORY.NumberOfNames
        // xor r9, r9
        DB(0x4D, 0x31, 0xC9);
        // mov r9d, [rax + 0x18]
        DB(0x44, 0x8B, 0x48, 0x18);

        // rdx = IMAGE_EXPORT_DIRECTORY.AddressOfNames + image base
        // xor rdx, rdx
        DB(0x48, 0x31, 0xD2);
        // mov edx, [rax + 0x20]
        DB(0x8B, 0x50, 0x20);
        // add rdx, rbx
        DB(0x48, 0x01, 0xDA);

        // registers:
        // rax = &IMAGE_EXPORT_DIRECTORY
        // rbx = image base
        // rdx = IMAGE_EXPORT_DIRECTORY.AddressOfNames + image base
        // r9 = IMAGE_EXPORT_DIRECTORY.NumberOfNames
        // r10 = lpProcName

        // r8 = 0 (loop index)
        // xor r8, r8
        DB(0x4D, 0x31, 0xC0);


        // check_next_function_name:

        // -- push 0
        // xor rcx, rcx
        DB(0x48, 0x31, 0xC9);
        // push rcx
        DB(0x51);

        // if index (r8) == NumberOfNames (r9)
        // cmp r8, r9
        DB(0x4D, 0x39, 0xC8);
        // je finished_search
        DB(0x74, 0x72);

        // add rsp, 8
        DB(0x48, 0x83, 0xC4, 0x08);


        // mov rdi, r8 (index)
        DB(0x4C, 0x89, 0xC7);
        // shl rdi, 2 // rdi *= sizeof(DWORD)
        DB(0x48, 0xC1, 0xE7, 0x02);
        // add rdi, rdx
        DB(0x48, 0x01, 0xD7);

        // mov edi, [rdi]
        DB(0x8B, 0x3F);
        // shl rdi, 32
        DB(0x48, 0xC1, 0xE7, 0x20);
        // shr rdi, 32
        DB(0x48, 0xC1, 0xEF, 0x20);
        // add rdi, rbx
        DB(0x48, 0x01, 0xDF);

        // mov rsi, r10
        DB(0x4C, 0x89, 0xD6);


        // mov r11, rdi
        DB(0x49, 0x89, 0xFB);
        // xor rcx, rcx
        DB(0x48, 0x31, 0xC9);
        // not rcx
        DB(0x48, 0xF7, 0xD1);
        // shl rax, 8
        DB(0x48, 0xC1, 0xE0, 0x08);
        // repne scasb
        DB(0xF2, 0xAE);
        // shr rax, 8
        DB(0x48, 0xC1, 0xE8, 0x08);
        // not rcx
        DB(0x48, 0xF7, 0xD1);
        // dec rcx
        DB(0x48, 0xFF, 0xC9);
        // mov rdi, r11
        DB(0x4C, 0x89, 0xDF);

        // repe cmpsb
        DB(0xF3, 0xA6);
        // je found_function_name
        DB(0x74, 0x05);

        // inc r8
        DB(0x49, 0xFF, 0xC0);
        // jmp check_next_function_name
        DB(0xEB, 0xB4);
        
        // found_function_name:

        // xor rdx, rdx
        DB(0x48, 0x31, 0xD2);
        // mov edx, [rax + 0x24] // AddressOfNameOrdinals
        DB(0x8B, 0x50, 0x24);
        // add rdx, rbx
        DB(0x48, 0x01, 0xDA);

        // shl r8, 1 // r8 *= sizeof(WORD)
        DB(0x49, 0xD1, 0xE0);
        // add rdx, r8
        DB(0x4C, 0x01, 0xC2);
        // xor r8, r8
        DB(0x4D, 0x31, 0xC0);
        // mov r8w, [rdx] // get ordinal
        DB(0x66, 0x44, 0x8B, 0x02);
        
        // xor rdx, rdx
        DB(0x48, 0x31, 0xD2);
        // mov edx, [rax + 0x1C] // AddressOfFunctions
        DB(0x8B, 0x50, 0x1C);
        // add rdx, rbx
        DB(0x48, 0x01, 0xDA);

        // shl r8, 2 // r8 *= sizeof(DWORD)
        DB(0x49, 0xC1, 0xE0, 0x02);
        // add rdx, r8
        DB(0x4C, 0x01, 0xC2);
        // xor rax, rax
        DB(0x48, 0x31, 0xC0);
        // mov eax, [rdx]
        DB(0x8B, 0x02);
        // add rax, rbx
        DB(0x48, 0x01, 0xd8);
        

        // push rax
        DB(0x50);

        // finished_search:
        __asm pop qw.LowPart

        X64PopA();
        SwitchBackToX86();

        retProcAddr = qw.QuadPart;
    }
    __finally
    {
    }

    return retProcAddr;
}

#endif // !_WIN64

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


FARPROC
WINAPI
GetProcAddressFromProcess(
    _In_opt_ HANDLE hProcess,
    _In_ HMODULE hModule,
    _In_ LPCSTR  lpProcName
)
{
    FARPROC retFarProc = NULL;
    BOOL apiStatus = FALSE;
    DWORD i = 0;
    DWORD j = 0;

    HANDLE hHeap = NULL;

    WORD ordinal = 0;

    DWORD_PTR remoteModuleBaseAdr = 0;

    BOOL is64BitModule = FALSE;
    BOOL useOrdinalNotName = FALSE;

    IMAGE_DOS_HEADER dosHeader = { 0 };
    DWORD signaturePE = 0;
    IMAGE_FILE_HEADER fileHeader = { 0 };
    IMAGE_OPTIONAL_HEADER32 optHeader32 = { 0 };
    IMAGE_OPTIONAL_HEADER64 optHeader64 = { 0 };
    IMAGE_DATA_DIRECTORY exportDirectory = { 0 };
    IMAGE_EXPORT_DIRECTORY exportTable = { 0 };

    FARPROC* exportFunctionTable = NULL;
    LPSTR* exportNameTable = NULL;
    WORD* exportOrdTable = NULL;

    BOOL functionNameFound = FALSE;
    LPSTR functionName = NULL;

    LPSTR realFunctionName = NULL;
    LPSTR realModuleName = NULL;
    HMODULE realHModule = NULL;
    WORD realOrdinal = 0;

    DWORD functionTableIndex = 0;

    __try
    {
        // if no process was given, 
        // then use the current process
        if (NULL == hProcess)
        {
            hProcess = GetCurrentProcess();
        }

        hHeap = GetProcessHeap();
        if (NULL == hHeap)
        {
            __leave;
        }

        /////////////////////////////////////////////////
        // start reading headers

        // get the remote base address of the module
        remoteModuleBaseAdr = (DWORD_PTR)hModule;

        // read the module's dos header
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)remoteModuleBaseAdr,
            &dosHeader,
            sizeof(IMAGE_DOS_HEADER),
            NULL
        );

        if (!apiStatus ||
            (IMAGE_DOS_SIGNATURE != dosHeader.e_magic))
        {
            __leave;
        }

        // read the module's pe signature
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)(remoteModuleBaseAdr + dosHeader.e_lfanew),
            &signaturePE,
            sizeof(signaturePE),
            NULL
        );

        if (!apiStatus ||
            (IMAGE_NT_SIGNATURE != signaturePE))
        {
            __leave;
        }

        // read the module's file header
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)(remoteModuleBaseAdr +
                dosHeader.e_lfanew + sizeof(signaturePE)),
            &fileHeader,
            sizeof(IMAGE_FILE_HEADER),
            NULL
        );

        if (!apiStatus)
        {
            __leave;
        }

        // check if the module is 32 or 64 bits
        if (fileHeader.SizeOfOptionalHeader ==
            sizeof(IMAGE_OPTIONAL_HEADER32))
        {
            is64BitModule = FALSE;
        }
        else if (fileHeader.SizeOfOptionalHeader ==
            sizeof(IMAGE_OPTIONAL_HEADER64))
        {
            is64BitModule = TRUE;
        }
        else
        {
            __leave;
        }

        // read the module's optional header
        if (!is64BitModule)
        {
            apiStatus = ReadProcessMemory(
                hProcess,
                (LPCVOID)(
                    remoteModuleBaseAdr +
                    dosHeader.e_lfanew +
                    sizeof(signaturePE) +
                    sizeof(IMAGE_FILE_HEADER)),
                &optHeader32,
                fileHeader.SizeOfOptionalHeader,
                NULL
            );

            if (!apiStatus ||
                (optHeader32.Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC))
            {
                __leave;
            }
        }
        else
        {
            apiStatus = ReadProcessMemory(
                hProcess,
                (LPCVOID)(
                    remoteModuleBaseAdr +
                    dosHeader.e_lfanew +
                    sizeof(signaturePE) +
                    sizeof(IMAGE_FILE_HEADER)),
                &optHeader64,
                fileHeader.SizeOfOptionalHeader,
                NULL
            );

            if (!apiStatus ||
                (optHeader64.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC))
            {
                __leave;
            }
        }

        // get the module's export data directory
        if (!is64BitModule &&
            (optHeader32.NumberOfRvaAndSizes >=
                IMAGE_DIRECTORY_ENTRY_EXPORT + 1))
        {
            exportDirectory =
                optHeader32.
                DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
        }
        else if (is64BitModule &&
            (optHeader64.NumberOfRvaAndSizes >=
                IMAGE_DIRECTORY_ENTRY_EXPORT + 1))
        {
            exportDirectory =
                optHeader64.
                DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
        }
        else
        {
            __leave;
        }

        // read the module's export directory
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)(
                remoteModuleBaseAdr +
                exportDirectory.VirtualAddress),
            &exportTable,
            sizeof(IMAGE_EXPORT_DIRECTORY),
            NULL
        );

        if (!apiStatus)
        {
            __leave;
        }

        // allocate space 
        // to read the array of function pointers
        exportFunctionTable = (FARPROC*)HeapAlloc(
            hHeap,
            HEAP_ZERO_MEMORY,
            exportTable.NumberOfFunctions * sizeof(FARPROC)
        );

        if (NULL == exportFunctionTable)
        {
            __leave;
        }

        // allocate space 
        // to read the array of function names
        exportNameTable = (LPSTR*)HeapAlloc(
            hHeap,
            HEAP_ZERO_MEMORY,
            exportTable.NumberOfNames * sizeof(LPSTR)
        );

        if (NULL == exportNameTable)
        {
            __leave;
        }

        // allocate space 
        // to read the array of function ordinals
        exportOrdTable = (WORD*)HeapAlloc(
            hHeap,
            HEAP_ZERO_MEMORY,
            exportTable.NumberOfNames * sizeof(WORD)
        );

        if (NULL == exportOrdTable)
        {
            __leave;
        }

        // read the module's exported function pointers
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)(
                remoteModuleBaseAdr +
                exportTable.AddressOfFunctions),
            exportFunctionTable,
            exportTable.NumberOfFunctions * sizeof(FARPROC),
            NULL
        );

        if (!apiStatus)
        {
            __leave;
        }

        // read the module's exported function names
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)(
                remoteModuleBaseAdr +
                exportTable.AddressOfNames),
            exportNameTable,
            exportTable.NumberOfNames * sizeof(LPSTR),
            NULL
        );

        if (!apiStatus)
        {
            __leave;
        }

        // read the module's exported function ordinals
        apiStatus = ReadProcessMemory(
            hProcess,
            (LPCVOID)(
                remoteModuleBaseAdr +
                exportTable.AddressOfNameOrdinals),
            exportOrdTable,
            exportTable.NumberOfNames * sizeof(WORD),
            NULL
        );

        if (!apiStatus)
        {
            __leave;
        }

        // finished reading headers and tables
        ////////////////////////////////////////////////////


        // check if ordinal is used
        if (0 == ((ULONG_PTR)lpProcName >> 16))
        {
            useOrdinalNotName = TRUE;
            ordinal = (WORD)((ULONG_PTR)lpProcName & 0xFFFF);

            // check if ordinal is valid
            if (ordinal < exportTable.Base ||
                ordinal - exportTable.Base >= exportTable.NumberOfFunctions)
            {
                __leave;
            }
        }

        if (!useOrdinalNotName)
        {

            if (NULL == functionName)
            {
                functionName = (LPSTR)HeapAlloc(
                    hHeap,
                    HEAP_ZERO_MEMORY,
                    4096
                );

                if (NULL == functionName)
                {
                    __leave;
                }
            }

            // look for the funcion with the given name
            for (i = 0; i < exportTable.NumberOfNames; i++)
            {
                functionNameFound = TRUE;
                for (j = 0; ; j++)
                {
                    apiStatus = ReadProcessMemory(
                        hProcess,
                        (LPCVOID)(
                            remoteModuleBaseAdr +
                            exportNameTable[i] + j),
                        &functionName[j],
                        sizeof(CHAR),
                        NULL
                    );

                    if (!apiStatus)
                    {
                        __leave;
                    }

                    if (functionName[j] == '\0')
                    {
                        if (lpProcName[j] != '\0')
                        {
                            functionNameFound = FALSE;
                        }

                        break;
                    }

                    if (functionName[j] != lpProcName[j])
                    {
                        functionNameFound = FALSE;
                        break;
                    }

                }

                if (functionNameFound)
                {
                    ordinal = exportOrdTable[i];
                    break;
                }
            } // for (i = 0; i < exportTable.NumberOfNames; i++)

            if (!functionNameFound)
            {
                __leave;
            }
        } // if (!useOrdinalNotName)


        // finished getting the function ordinal
        ////////////////////////////////////////

        // functionTableIndex = ordinal - exportTable.Base;
        functionTableIndex = (DWORD)ordinal;
        retFarProc =
            (FARPROC)
            (remoteModuleBaseAdr +
            (ULONG_PTR)exportFunctionTable[functionTableIndex]);

        // if function is forwarded
        if (((ULONG_PTR)exportFunctionTable[functionTableIndex] >= exportDirectory.VirtualAddress) &&
            ((ULONG_PTR)exportFunctionTable[functionTableIndex] <
                exportDirectory.VirtualAddress + exportDirectory.Size))
        {

            if (NULL == functionName)
            {
                functionName = (LPSTR)HeapAlloc(
                    hHeap,
                    HEAP_ZERO_MEMORY,
                    4096
                );

                if (NULL == functionName)
                {
                    __leave;
                }
            }

            // read function forwarding name
            for (j = 0; ; j++)
            {
                apiStatus = ReadProcessMemory(
                    hProcess,
                    (LPCVOID)(
                        remoteModuleBaseAdr +
                        (ULONG_PTR)exportFunctionTable[functionTableIndex] +
                        j),
                    &functionName[j],
                    sizeof(CHAR),
                    NULL
                );

                if (!apiStatus)
                {
                    __leave;
                }

                if (functionName[j] == '\0')
                {
                    break;
                }
            } // read function name

            functionNameFound = FALSE;
            for (j = 0; ; j++)
            {
                if (functionName[j] == '.')
                {
                    realModuleName = functionName;
                    realFunctionName = &functionName[j + 1];
                    functionName[j] = '\0';
                    functionNameFound = TRUE;
                    break;
                }
            }

            if (!functionNameFound)
            {
                __leave;
            }

            realHModule = GetRemoteModuleHandleA(
                hProcess,
                realModuleName,
                MODULE_ANY_BITNESS
            );

            // export forwarded by ordinal
            if ('#' == realFunctionName[0])
            {
                realOrdinal = (WORD)atoi(&realFunctionName[1]);

                retFarProc = GetProcAddressFromProcess(
                    hProcess,
                    realHModule,
                    MAKEINTRESOURCEA(realOrdinal)
                );
            }
            else
            {
                retFarProc = GetProcAddressFromProcess(
                    hProcess,
                    realHModule,
                    realFunctionName
                );
            }


        } // if function is forwarded
    }
    __finally
    {
        if (NULL != exportFunctionTable)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                exportFunctionTable
            );

            if (!apiStatus)
            {
                ;
            }

            exportFunctionTable = NULL;
        }

        if (NULL != exportNameTable)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                exportNameTable
            );

            if (!apiStatus)
            {
                ;
            }

            exportNameTable = NULL;
        }

        if (NULL != exportOrdTable)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                exportOrdTable
            );

            if (!apiStatus)
            {
                ;
            }

            exportOrdTable = NULL;
        }


        if (NULL != functionName)
        {
            apiStatus = HeapFree(
                hHeap,
                0,
                functionName
            );

            if (!apiStatus)
            {
                ;
            }

            functionName = NULL;
        }

    }

    return retFarProc;
}

BOOL
WINAPI
IsWindows64Bit(
    _Out_ PBOOL  Windows64Bit
)
{
    BOOL apiStatus = FALSE;

    BOOL win64 = FALSE;

    BOOL wow64 = FALSE;
    DWORD pointerSize = sizeof(void*);

    __try
    {
        apiStatus = IsWow64Process(
            GetCurrentProcess(),
            &wow64);

        if (!apiStatus)
        {
            __leave;
        }

        if (wow64 || (8 == pointerSize))
        {
            win64 = TRUE;
        }

        *Windows64Bit = win64;
        apiStatus = TRUE;
    }
    __finally
    {
    }

    return apiStatus;
}
