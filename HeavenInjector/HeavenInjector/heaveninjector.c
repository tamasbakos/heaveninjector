//
//   Copyright (C) 2016 BitDefender S.R.L.
//   Author(s)    : Tamas BAKOS(tbakos@bitdefender.com)
//

#include "win_intern.h"
#include <stdio.h>

typedef NTSTATUS(NTAPI *pNtQueryInformationProcess)(
    IN  HANDLE ProcessHandle,
    IN  PROCESSINFOCLASS ProcessInformationClass,
    OUT PVOID ProcessInformation,
    IN  ULONG ProcessInformationLength,
    OUT PULONG ReturnLength    OPTIONAL
    );



int main()
{
    //__debugbreak();

    QWORD hModuleNtDll = 0;
    ULARGE_INTEGER qw = { 0 };

    /*
    PROCESS_BASIC_INFORMATION procInfo = { 0 };
    NTSTATUS ntStatus = 0;

    HMODULE hNtDll = NULL;
    HMODULE hMod = NULL;
    pNtQueryInformationProcess NtQueryInformationProcess = NULL;

    NT_TIB64* tib64;

    FARPROC procadr = NULL;
    FARPROC procadr2 = NULL;

    hNtDll = LoadLibraryW(L"ntdll.dll");

    NtQueryInformationProcess =
        (pNtQueryInformationProcess)GetProcAddress(
            hNtDll,
            "NtQueryInformationProcess");

    ntStatus = NtQueryInformationProcess(
        GetCurrentProcess(),
        ProcessBasicInformation,
        &procInfo,
        sizeof(PROCESS_BASIC_INFORMATION),
        NULL
    );
*/

/*
    hMod = GetRemoteModuleHandleA(
        GetCurrentProcess(),
        "kernel32.dll"
    );

    if (hMod == GetModuleHandleA("kernel32.dll"))
    {
        printf("good\n");
    }

    procadr = GetRemoteProcAddress(
        GetCurrentProcess(),
        hMod,
        "LoadLibraryW"
    );

    procadr2 = GetProcAddress(hMod, "LoadLibraryW");

    if(procadr == procadr2)
    {
        printf("good2\n");
    }

    PVOID oldValue;

    BOOL apiStatus;
    DWORD err;

    apiStatus = Wow64DisableWow64FsRedirection(&oldValue);

    if (!apiStatus)
    {
        err = GetLastError();
    }
*/

#ifdef _WIN64

    DWORD pid;
    HANDLE hProcess = NULL;

    QWORD hMod = //0x00007FFAF0990000;

        GetRemoteModuleHandleW(hProcess, L"ntdll.dll", MODULE_32BIT);

    hMod = GetModuleHandleA("ntdll.dll");

    QWORD pProc = GetProcAddress(hMod, "LdrLoadDll");

    qw.QuadPart = hMod;
    printf("ntdll.dll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);
    qw.QuadPart = pProc;
    printf("LdrLoadDll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    /*
    printf("Pid: ");
    scanf_s("%d", &pid);
    hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
    if (NULL == hProcess)
    {
        return 1;
    }
    

    IMAGE_DOS_HEADER dosH;
    IMAGE_NT_HEADERS64 ntH;

    ReadProcessMemory(hProcess, hMod, &dosH, sizeof(IMAGE_DOS_HEADER), NULL);

    printf("%c%c\n", ((BYTE*)&dosH.e_magic)[0], ((BYTE*)&dosH.e_magic)[1]);

    ReadProcessMemory(hProcess, hMod + dosH.e_lfanew, &ntH, sizeof(IMAGE_NT_HEADERS64), NULL);
    

    CloseHandle(hProcess);

    */

#else

    hModuleNtDll = Wow64GetNativeModuleHandleW(L"ntdll.dll");
    qw.QuadPart = hModuleNtDll;
    printf("ntdll.dll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    qw.QuadPart = Wow64GetNativeProcAddress(hModuleNtDll, "LdrLoadDll");
    printf("LdrLoadDll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    qw.QuadPart = Wow64GetNativeProcAddress(hModuleNtDll, "LdrLoadD");
    printf("LdrLoadD: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    /*
    hModuleNtDll = Wow64GetNativeModuleHandleW(L"wow64.dll");
    qw.QuadPart = hModuleNtDll;
    printf("wow64.dll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    hModuleNtDll = Wow64GetNativeModuleHandleW(L"wow64win.dll");
    qw.QuadPart = hModuleNtDll;
    printf("wow64win.dll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    hModuleNtDll = Wow64GetNativeModuleHandleW(L"wow64cpu.dll");
    qw.QuadPart = hModuleNtDll;
    printf("wow64cpu.dll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);

    hModuleNtDll = Wow64GetNativeModuleHandleW(L"wrong.dll");
    qw.QuadPart = hModuleNtDll;
    printf("wrong.dll: 0x%08X'%08X\n", qw.HighPart, qw.LowPart);
    */
    
#endif // _Win64

    return 0;
}
