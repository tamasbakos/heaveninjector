#ifndef _WINDOWS_EX_H_
#define _WINDOWS_EX_H_

//#include <winternl.h>
#include <Windows.h>


typedef DWORD64 QWORD;

#define MODULE_DEFAULT 0x0  // This is the default one app would get without any flag.
#define MODULE_32BIT   0x01  // list 32bit modules in the target process.
#define MODULE_64BIT   0x02  // list all 64bit modules. 32bit exe will be stripped off.

// list all the modules
#define MODULE_ANY_BITNESS   (MODULE_32BIT | MODULE_64BIT)

typedef enum _PROCESSINFOCLASS
{
    ProcessBasicInformation = 0,
    ProcessDebugPort = 7,
    ProcessWow64Information = 26,
    ProcessImageFileName = 27,
    ProcessBreakOnTermination = 29,
    ProcessSubsystemInformation = 75

} PROCESSINFOCLASS;

typedef enum _LDR_DLL_LOAD_REASON
{
    LoadReasonStaticDependency,
    LoadReasonStaticForwarderDependency,
    LoadReasonDynamicForwarderDependency,
    LoadReasonDelayloadDependency,
    LoadReasonDynamicLoad,
    LoadReasonAsImageLoad,
    LoadReasonAsDataLoad,
    LoadReasonUnknown = -1

} LDR_DLL_LOAD_REASON, *PLDR_DLL_LOAD_REASON;

typedef NTSTATUS(NTAPI *pNtQueryInformationProcess)(
    IN  HANDLE ProcessHandle,
    IN  PROCESSINFOCLASS ProcessInformationClass,
    OUT PVOID ProcessInformation,
    IN  ULONG ProcessInformationLength,
    OUT PULONG ReturnLength    OPTIONAL
    );

#pragma warning( push )
#pragma warning( disable : 4214) // nonstandard extension used: bit field types other than int
#pragma warning( disable : 4201) // nonstandard extension used: nameless struct/union

/*
    Unicode String
*/

typedef struct _UNICODE_STRING
{
    WORD        Length;
    WORD        MaximumLength;
    PWSTR       Buffer;

} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _UNICODE_STRING32
{
    WORD        Length;
    WORD        MaximumLength;
    DWORD       Buffer;

} UNICODE_STRING32, *PUNICODE_STRING32;

typedef struct _UNICODE_STRING64
{
    WORD        Length;
    WORD        MaximumLength;
    QWORD       Buffer;

} UNICODE_STRING64, *PUNICODE_STRING64;

/*
    String
*/

typedef struct _STRING
{
    WORD        Length;
    WORD        MaximumLength;
    PCHAR       Buffer;

} STRING, *PSTRING;

typedef struct _STRING32
{
    WORD        Length;
    WORD        MaximumLength;
    DWORD       Buffer;

} STRING32, *PSTRING32;

typedef struct _STRING64
{
    WORD        Length;
    WORD        MaximumLength;
    QWORD       Buffer;

} STRING64, *PSTRING64;


/*
// List Entry

typedef struct _LIST_ENTRY
{
    struct _LIST_ENTRY *Flink;
    struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;

typedef struct _LIST_ENTRY32
{
    DWORD Flink;
    DWORD Blink;
} LIST_ENTRY32, *PLIST_ENTRY32;

typedef struct _LIST_ENTRY64
{
    QWORD Flink;
    QWORD Blink;
} LIST_ENTRY64, *PLIST_ENTRY64;
*/

typedef struct _RTL_BALANCED_NODE
{
    union
    {
        struct _RTL_BALANCED_NODE *Children[2];
        struct
        {
            struct _RTL_BALANCED_NODE *Left;
            struct _RTL_BALANCED_NODE *Right;
        };
    };
    union
    {
        UCHAR Red : 1;
        UCHAR Balance : 2;
        ULONG_PTR ParentValue;
    };
} RTL_BALANCED_NODE, *PRTL_BALANCED_NODE;

typedef struct _RTL_BALANCED_NODE32
{
    union
    {
        DWORD Children[2];
        struct
        {
            DWORD Left;
            DWORD Right;
        };
    };
    union
    {
        UCHAR Red : 1;
        UCHAR Balance : 2;
        DWORD ParentValue;
    };
} RTL_BALANCED_NODE32, *PRTL_BALANCED_NODE32;

typedef struct _RTL_BALANCED_NODE64
{
    union
    {
        QWORD Children[2];
        struct
        {
            QWORD Left;
            QWORD Right;
        };
    };
    union
    {
        UCHAR Red : 1;
        UCHAR Balance : 2;
        QWORD ParentValue;
    };
} RTL_BALANCED_NODE64, *PRTL_BALANCED_NODE64;

/* Official
typedef struct _LDR_DATA_TABLE_ENTRY
{
    PVOID Reserved1[2];  // InLoadOrderModuleList
    LIST_ENTRY InMemoryOrderLinks;
    PVOID Reserved2[2];  // InInitializationOrderModuleList
    PVOID DllBase;  // BaseAddress
    PVOID Reserved3[2];  // EntryPoint + SizeOfImage
    UNICODE_STRING FullDllName;
    BYTE Reserved4[8]; // BaseDllName
    PVOID Reserved5[3];
    union {
        ULONG CheckSum;
        PVOID Reserved6;
    } DUMMYUNIONNAME;
    ULONG TimeDateStamp;
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;
*/

// typedef struct _LDR_DATA_TABLE_ENTRY 
typedef struct _LDR_MODULE
{
    LIST_ENTRY          InLoadOrderModuleList;
    LIST_ENTRY          InMemoryOrderModuleList;
    LIST_ENTRY          InInitializationOrderModuleList;
    PVOID               BaseAddress;
    PVOID               EntryPoint;
    ULONG               SizeOfImage;
    UNICODE_STRING      FullDllName;
    UNICODE_STRING      BaseDllName;
    ULONG               Flags;
    SHORT               LoadCount;
    SHORT               TlsIndex;
    LIST_ENTRY          HashTableEntry;
    ULONG               TimeDateStamp;
    PVOID               EntryPointActivationContext;
    PVOID               Lock;
    PVOID               DdagNode;
    LIST_ENTRY          NodeModuleLink;
    PVOID               LoadContext;
    PVOID               ParentDllBase;
    PVOID               SwitchBackContext;
    RTL_BALANCED_NODE   BaseAddressIndexNode;
    RTL_BALANCED_NODE   MappingInfoIndexNode;
    ULONG_PTR           OriginalBase;
    LARGE_INTEGER       LoadTime;
    DWORD               BaseNameHashValue;
    LDR_DLL_LOAD_REASON LoadReason;
    DWORD               ImplicitPathOptions;
    DWORD               ReferenceCount;

} LDR_MODULE, *PLDR_MODULE;

typedef struct _LDR_MODULE32
{
    LIST_ENTRY32        InLoadOrderModuleList;
    LIST_ENTRY32        InMemoryOrderModuleList;
    LIST_ENTRY32        InInitializationOrderModuleList;
    DWORD               BaseAddress;
    DWORD               EntryPoint;
    ULONG               SizeOfImage;
    UNICODE_STRING32    FullDllName;
    UNICODE_STRING32    BaseDllName;
    ULONG               Flags;
    SHORT               LoadCount;
    SHORT               TlsIndex;
    LIST_ENTRY32        HashTableEntry;
    ULONG               TimeDateStamp;
    DWORD               EntryPointActivationContext;
    DWORD               Lock;
    DWORD               DdagNode;
    LIST_ENTRY32        NodeModuleLink;
    DWORD               LoadContext;
    DWORD               ParentDllBase;
    DWORD               SwitchBackContext;
    RTL_BALANCED_NODE32 BaseAddressIndexNode;
    RTL_BALANCED_NODE32 MappingInfoIndexNode;
    DWORD               OriginalBase;
    LARGE_INTEGER       LoadTime;
    DWORD               BaseNameHashValue;
    LDR_DLL_LOAD_REASON LoadReason;
    DWORD               ImplicitPathOptions;
    DWORD               ReferenceCount;

} LDR_MODULE32, *PLDR_MODULE32;

typedef struct _LDR_MODULE64
{
    LIST_ENTRY64        InLoadOrderModuleList;
    LIST_ENTRY64        InMemoryOrderModuleList;
    LIST_ENTRY64        InInitializationOrderModuleList;
    QWORD               BaseAddress;
    QWORD               EntryPoint;
    ULONG               SizeOfImage;
    UNICODE_STRING64    FullDllName;
    UNICODE_STRING64    BaseDllName;
    ULONG               Flags;
    SHORT               LoadCount;
    SHORT               TlsIndex;
    LIST_ENTRY64        HashTableEntry;
    ULONG               TimeDateStamp;
    QWORD               EntryPointActivationContext;
    QWORD               Lock;
    QWORD               DdagNode;
    LIST_ENTRY64        NodeModuleLink;
    QWORD               LoadContext;
    QWORD               ParentDllBase;
    QWORD               SwitchBackContext;
    RTL_BALANCED_NODE64 BaseAddressIndexNode;
    RTL_BALANCED_NODE64 MappingInfoIndexNode;
    QWORD               OriginalBase;
    LARGE_INTEGER       LoadTime;
    DWORD               BaseNameHashValue;
    LDR_DLL_LOAD_REASON LoadReason;
    DWORD               ImplicitPathOptions;
    DWORD               ReferenceCount;

} LDR_MODULE64, *PLDR_MODULE64;

/* Official
typedef struct _PEB_LDR_DATA {
BYTE Reserved1[8]; // dw[2]
PVOID Reserved2[3];
LIST_ENTRY InMemoryOrderModuleList;
} PEB_LDR_DATA, *PPEB_LDR_DATA;
*/

typedef struct _PEB_LDR_DATA
{
    ULONG           Length;
    BOOLEAN         Initialized;
    PVOID           SsHandle;
    LIST_ENTRY      InLoadOrderModuleList;
    LIST_ENTRY      InMemoryOrderModuleList;
    LIST_ENTRY      InInitializationOrderModuleList;
    PVOID           EntryInProgress;
    BOOLEAN         ShutdownInProgress;
    PVOID           ShutdownThreadId;

} PEB_LDR_DATA, *PPEB_LDR_DATA;

typedef struct _PEB_LDR_DATA32
{
    ULONG           Length;
    BOOLEAN         Initialized;
    DWORD           SsHandle;
    LIST_ENTRY32    InLoadOrderModuleList;
    LIST_ENTRY32    InMemoryOrderModuleList;
    LIST_ENTRY32    InInitializationOrderModuleList;
    DWORD           EntryInProgress;
    BOOLEAN         ShutdownInProgress;
    DWORD           ShutdownThreadId;

} PEB_LDR_DATA32, *PPEB_LDR_DATA32;

typedef struct _PEB_LDR_DATA64
{
    ULONG           Length;
    BOOLEAN         Initialized;
    QWORD           SsHandle;
    LIST_ENTRY64    InLoadOrderModuleList;
    LIST_ENTRY64    InMemoryOrderModuleList;
    LIST_ENTRY64    InInitializationOrderModuleList;
    QWORD           EntryInProgress;
    BOOLEAN         ShutdownInProgress;
    QWORD           ShutdownThreadId;

} PEB_LDR_DATA64, *PPEB_LDR_DATA64;

/*
    Process Environment Block
*/

typedef struct _CURDIR
{
    UNICODE_STRING  DosPath;
    PVOID           Handle;

} CURDIR, *PCURDIR;

typedef struct _CURDIR32
{
    UNICODE_STRING32  DosPath;
    DWORD             Handle;

} CURDIR32, *PCURDIR32;

typedef struct _CURDIR64
{
    UNICODE_STRING64  DosPath;
    QWORD             Handle;

} CURDIR64, *PCURDIR64;

typedef struct _RTL_DRIVE_LETTER_CURDIR
{
    USHORT      Flags;
    USHORT      Length;
    DWORD       TimeStamp;
    STRING      DosPath;

} RTL_DRIVE_LETTER_CURDIR, *PRTL_DRIVE_LETTER_CURDIR;


typedef struct _RTL_DRIVE_LETTER_CURDIR32
{
    USHORT      Flags;
    USHORT      Length;
    DWORD       TimeStamp;
    STRING32    DosPath;

} RTL_DRIVE_LETTER_CURDIR32, *PRTL_DRIVE_LETTER_CURDIR32;


typedef struct _RTL_DRIVE_LETTER_CURDIR64
{
    USHORT      Flags;
    USHORT      Length;
    DWORD       TimeStamp;
    STRING64    DosPath;

} RTL_DRIVE_LETTER_CURDIR64, *PRTL_DRIVE_LETTER_CURDIR64;

typedef struct _CLIENT_ID
{
    PVOID       UniqueProcess;
    PVOID       UniqueThread;

} CLIENT_ID, *PCLIENT_ID;

typedef struct _CLIENT_ID32
{
    DWORD       UniqueProcess;
    DWORD       UniqueThread;

} CLIENT_ID32, *PCLIENT_ID32;

typedef struct _CLIENT_ID64
{
    QWORD       UniqueProcess;
    QWORD       UniqueThread;

} CLIENT_ID64, *PCLIENT_ID64;

/*
typedef struct _RTL_USER_PROCESS_PARAMETERS
{
    BYTE Reserved1[16];
    PVOID Reserved2[10];
    UNICODE_STRING ImagePathName;
    UNICODE_STRING CommandLine;
    // To do: complete structure
} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;
*/

typedef struct _RTL_USER_PROCESS_PARAMETERS
{
    DWORD                   MaximumLength;
    DWORD                   Length;
    DWORD                   Flags;
    DWORD                   DebugFlags;
    PVOID                   ConsoleHandle;
    DWORD                   ConsoleFlags;
    PVOID                   StandardInput;
    PVOID                   StandardOutput;
    PVOID                   StandardError;
    CURDIR                  CurrentDirectory;
    UNICODE_STRING          DllPath;
    UNICODE_STRING          ImagePathName;
    UNICODE_STRING          CommandLine;
    PVOID                   Environment;
    DWORD                   StartingX;
    DWORD                   StartingY;
    DWORD                   CountX;
    DWORD                   CountY;
    DWORD                   CountCharsX;
    DWORD                   CountCharsY;
    DWORD                   FillAttribute;
    DWORD                   WindowFlags;
    DWORD                   ShowWindowFlags;
    UNICODE_STRING          WindowTitle;
    UNICODE_STRING          DesktopInfo;
    UNICODE_STRING          ShellInfo;
    UNICODE_STRING          RuntimeData;
    RTL_DRIVE_LETTER_CURDIR CurrentDirectores[32];
    ULONG_PTR               EnvironmentSize;
    ULONG_PTR               EnvironmentVersion;
    PVOID                   PackageDependencyData;
    DWORD                   ProcessGroupId;
    DWORD                   LoaderThreads;

} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;

typedef struct _RTL_ACTIVATION_CONTEXT_STACK_FRAME
{
    struct _RTL_ACTIVATION_CONTEXT_STACK_FRAME * Previous;
    struct _ACTIVATION_CONTEXT * ActivationContext;
    ULONG Flags;
} RTL_ACTIVATION_CONTEXT_STACK_FRAME, *PRTL_ACTIVATION_CONTEXT_STACK_FRAME;

typedef struct _ACTIVATION_CONTEXT_STACK
{
    PRTL_ACTIVATION_CONTEXT_STACK_FRAME ActiveFrame;
    LIST_ENTRY FrameListCache;
    ULONG Flags;
    ULONG NextCookieSequenceNumber;
    ULONG StackId;
} ACTIVATION_CONTEXT_STACK, *PACTIVATION_CONTEXT_STACK;

typedef struct _GDI_TEB_BATCH
{
    DWORD       Offset : 31;
    DWORD       HasRenderingCommand : 1;
    DWORD_PTR   HDC;
    DWORD       Buffer[310];

} GDI_TEB_BATCH, *PGDI_TEB_BATCH;


// http://www.geoffchappell.com/studies/windows/win32/ntdll/structs/peb/index.htm

typedef struct _PEB
{                                                           // x86    // x64
    BOOLEAN         InheritedAddressSpace;                  // +0x000 // +0x000
    BOOLEAN         ReadImageFileExecOptions;               // +0x001 // +0x001
    BOOLEAN         BeingDebugged;                          // +0x002 // +0x002
    union {
        BYTE BitField;
        struct {
            BYTE ImageUsedLargePages : 1;
            BYTE IsProtectedProcess : 1;
            BYTE IsImageDynamicallyRelocated : 1;
            BYTE SkipPatchingUser32Forwarders : 1;
            BYTE IsPackagedProcess : 1;
            BYTE IsAppContainer : 1;
            BYTE IsProtectedProcessLight : 1;
            BYTE IsLongPathAwareProcess : 1;
        };
    };

#if defined(_WIN64) // x64
    BYTE            Padding0[4];                                      // +0x004
#endif

    PVOID           Mutant;                                 // +0x004 // +0x008
    PVOID           ImageBaseAddress;                       // +0x008 // +0x010
    PPEB_LDR_DATA   Ldr;                                    // +0x00c // +0x018
    PRTL_USER_PROCESS_PARAMETERS ProcessParameters;         // +0x010 // +0x020
    PVOID           SubSystemData;                          // +0x014 // +0x028
    PVOID           ProcessHeap;                            // +0x018 // +0x030
    PRTL_CRITICAL_SECTION FastPebLock;                      // +0x01c // +0x038
    PVOID           AtlThunkSListPtr;                       // +0x020 // +0x040
    PVOID           IFEOKey;                                // +0x024 // +0x048
    union {
        DWORD       CrossProcessFlags;
        struct {
            DWORD   ProcessInJob : 1;
            DWORD   ProcessInitializing : 1;
            DWORD   ProcessUsingVEH : 1;
            DWORD   ProcessUsingVCH : 1;
            DWORD   ProcessUsingFTH : 1;
            DWORD   ReservedBits0 : 27;
        };
    };
#if defined(_WIN64) // x64
    BYTE            Padding1[4];                                      // +0x054
#endif

    union {
        PVOID       KernelCallbackTable;                    // +0x02c // +0x058
        PVOID       UserSharedInfoPtr;                      // +0x02c // +0x058
    };
    DWORD           SystemReserved[1];                      // +0x030 // +0x060
    DWORD           AtlThunkSListPtr32;                     // +0x034 // +0x064
    PVOID           ApiSetMap;                              // +0x038 // +0x068

    DWORD TlsExpansionCounter;                              // +0x03c // +0x070
#if defined(_WIN64) // x64
    BYTE            Padding2[4];                                      // +0x074
#endif
    PVOID           TlsBitmap;                              // +0x040 // +0x078
    DWORD           TlsBitmapBits[2];                       // +0x044 // +0x080
    PVOID           ReadOnlySharedMemoryBase;               // +0x04c // +0x088
    PVOID           HotpatchInformation;                    // +0x050 // +0x090
    PVOID*          ReadOnlyStaticServerData;               // +0x054 // +0x098
    PVOID           AnsiCodePageData;                       // +0x058 // +0x0a0
    PVOID           OemCodePageData;                        // +0x05c // +0x0a8
    PVOID           UnicodeCaseTableData;                   // +0x060 // +0x0b0
    DWORD           NumberOfProcessors;                     // +0x064 // +0x0b8
    DWORD           NtGlobalFlag;                           // +0x068 // +0x0bc
    LARGE_INTEGER   CriticalSectionTimeout;                 // +0x070 // +0x0c0
    DWORD_PTR       HeapSegmentReserve;                     // +0x078 // +0x0c8
    DWORD_PTR       HeapSegmentCommit;                      // +0x07c // +0x0d0
    DWORD_PTR       HeapDeCommitTotalFreeThreshold;         // +0x080 // +0x0d8
    DWORD_PTR       HeapDeCommitFreeBlockThreshold;         // +0x084 // +0x0e0
    DWORD           NumberOfHeaps;                          // +0x088 // +0x0e8
    DWORD           MaximumNumberOfHeaps;                   // +0x08c // +0x0ec
    PVOID*          ProcessHeaps;                           // +0x090 // +0x0f0
    PVOID           GdiSharedHandleTable;                   // +0x094 // +0x0f8

    PVOID           ProcessStarterHelper;                   // +0x098 // +0x100
    DWORD           GdiDCAttributeList;                     // +0x09c // +0x108
#if defined(_WIN64) // x64
    BYTE            Padding3[4];                                      // +0x10c
#endif
    RTL_CRITICAL_SECTION *LoaderLock;                       // +0x0a0 // +0x110
    DWORD           OSMajorVersion;                         // +0x0a4 // +0x118
    DWORD           OSMinorVersion;                         // +0x0a8 // +0x11c
    WORD            OSBuildNumber;                          // +0x0ac // +0x120
    WORD            OSCSDVersion;                           // +0x0ae // +0x122
    DWORD           OSPlatformId;                           // +0x0b0 // +0x124
    DWORD           ImageSubsystem;                         // +0x0b4 // +0x128
    DWORD           ImageSubsystemMajorVersion;             // +0x0b8 // +0x12c
    DWORD           ImageSubsystemMinorVersion;             // +0x0bc // +0x130
#if defined(_WIN64) // x64
    BYTE            Padding4[4];                                      // +0x134
#endif
    KAFFINITY       ActiveProcessAffinityMask;              // +0x0c0 // +0x138

#if !defined(_WIN64) // x86
    DWORD           GdiHandleBuffer[0x22];//[34];           // +0x0c4 
#else // x64
    DWORD           GdiHandleBuffer[0x3C];//[60];                     // +0x140
#endif
    VOID(*PostProcessInitRoutine) (VOID);                   // +0x14c // +0x230

    PVOID           TlsExpansionBitmap;                     // +0x150 // +0x238
    DWORD           TlsExpansionBitmapBits[0x20];//[32];    // +0x154 // +0x240
    DWORD           SessionId;                              // +0x1d4 // +0x2c0
#if defined(_WIN64) // x64
    BYTE            Padding5[4];                                      // +0x2c4
#endif

    ULARGE_INTEGER  AppCompatFlags;                         // +0x1d8 // +0x2c8
    ULARGE_INTEGER  AppCompatFlagsUser;                     // +0x1e0 // +0x2d0
    PVOID           pShimData;                              // +0x1e8 // +0x2d8
    PVOID           AppCompatInfo;                          // +0x1ec // +0x2e0
    UNICODE_STRING  CSDVersion;                             // +0x1f0 // +0x2e8

    struct _ACTIVATION_CONTEXT_DATA*   ActivationContextData;
    struct _ASSEMBLY_STORAGE_MAP*      ProcessAssemblyStorageMap;
    struct _ACTIVATION_CONTEXT_DATA*   SystemDefaultActivationContextData;
    struct _ASSEMBLY_STORAGE_MAP*      SystemAssemblyStorageMap;
    DWORD_PTR       MinimumStackCommit;

    // To be continued
} PEB, *PPEB;

static const SIZE_T offs = offsetof(PEB, MinimumStackCommit);

typedef struct _PEB32
{
    BYTE            InheritedAddressSpace;
    BYTE            ReadImageFileExecOptions;
    BOOLEAN         BeingDebugged; // +0x2
    BYTE            BitField; // +0x3
    DWORD           Mutant;           // +0x4(x86) +0x8(x64)
    DWORD           ImageBaseAddress; // +0x8(x86) +0x10(x64)
    DWORD           Ldr; // PPEB_LDR_DATA32
    DWORD           ProcessParameters; // PRTL_USER_PROCESS_PARAMETERS
    DWORD           SubSystemData;
    DWORD           ProcessHeap;
    DWORD           FastPebLock; // PRTL_CRITICAL_SECTION
    DWORD           AtlThunkSListPtr;
    DWORD           IFEOKey;
    ULONG           CrossProcessFlags;
    DWORD           KernelCallbackTable;
    ULONG           SystemReserved;
    ULONG           AtlThunkSListPtr32;
    DWORD           Reserved9[45];
    BYTE            Reserved10[96];
    DWORD           PostProcessInitRoutine; // PPS_POST_PROCESS_INIT_ROUTINE
    BYTE            Reserved11[128];
    DWORD           Reserved12[1];
    ULONG           SessionId;
    // To be continued
} PEB32, *PPEB32;

typedef struct _PEB64
{
    BYTE InheritedAddressSpace;
    BYTE ReadImageFileExecOptions;
    BOOLEAN BeingDebugged; // +0x2
    BYTE BitField; // +0x3
    QWORD Mutant;           // +0x4(x86) +0x8(x64)
    QWORD ImageBaseAddress; // +0x8(x86) +0x10(x64)
    QWORD Ldr; // PPEB_LDR_DATA64
    QWORD ProcessParameters; // PRTL_USER_PROCESS_PARAMETERS
    QWORD SubSystemData;
    QWORD ProcessHeap;
    QWORD FastPebLock; // PRTL_CRITICAL_SECTION
    QWORD AtlThunkSListPtr;
    QWORD IFEOKey;
    ULONG CrossProcessFlags;
    QWORD KernelCallbackTable;
    ULONG SystemReserved;
    ULONG AtlThunkSListPtr32;
    QWORD Reserved9[45];
    BYTE Reserved10[96];
    QWORD PostProcessInitRoutine; // PPS_POST_PROCESS_INIT_ROUTINE
    BYTE Reserved11[128];
    QWORD Reserved12[1];
    ULONG SessionId;
    // To be continued
} PEB64, *PPEB64;

// http://www.geoffchappell.com/studies/windows/win32/ntdll/structs/teb/index.htm

typedef struct _TEB
{                                                            // x86    // x64
    NT_TIB          NtTib;                                   // +0x000 // +0x000
    PVOID           EnvironmentPointer;                      // +0x01c // +0x038
    CLIENT_ID       ClientId;                                // +0x020 // +0x040
    PVOID           ActiveRpcHandle;                         // +0x028 // +0x050
    PVOID           ThreadLocalStoragePointer;               // +0x02c // +0x058
    PPEB            ProcessEnvironmentBlock;                 // +0x030 // +0x060
    DWORD           LastErrorValue;                          // +0x034 // +0x068
    DWORD           CountOfOwnedCriticalSections;            // +0x038 // +0x06c
    PVOID           CsrClientThread;                         // +0x03c // +0x070
    PVOID           Win32ThreadInfo;                         // +0x040 // +0x078
    DWORD           User32Reserved[0x1A];                    // +0x044 // +0x080
    DWORD           UserReserved[5];                         // +0x0ac // +0x0e8
    PVOID           WOW32Reserved;                           // +0x0c0 // +0x100
    DWORD           CurrentLocale;                           // +0x0c4 // +0x108
    DWORD           FpSoftwareStatusRegister;                // +0x0c8 // +0x10c

    PVOID           ReservedForDebuggerInstrumentation[0x10];// +0x0cc // +0x110

#if defined(_WIN64) // x64
    PVOID           SystemReserved1[0x25];                   //        // +0x190
#else // x86
    PVOID           SystemReserved1[0x24];                   // +0x10c //
#endif

    BYTE            WorkingOnBehalfTicket[8];                // +0x19c // +0x2b8
    INT             ExceptionCode;                           // +0x1a4 // +0x2c0

#if defined(_WIN64) // x64
    BYTE            Padding0[4];                                       // +0x2c4
#endif

    PACTIVATION_CONTEXT_STACK ActivationContextStackPointer; // +0x1a8 // +0x2c8
    DWORD_PTR       InstrumentationCallbackSp;               // +0x1ac // +0x2d0
    DWORD_PTR       InstrumentationCallbackPreviousPc;       // +0x1b0 // +0x2d8
    DWORD_PTR       InstrumentationCallbackPreviousSp;       // +0x1b4 // +0x2e0

#if defined(_WIN64) // x64
    DWORD           TxFsContext;                             //        // +0x2e8
#endif
    BOOLEAN         InstrumentationCallbackDisabled;         // +0x1b8 // +0x2ec

#if !defined(_WIN64) // x86
    BYTE            SpareBytes[0x17];//[23];                 // +0x1b9
    DWORD           TxFsContext;                             // +0x1d0
#else // x64
    BYTE            Padding1[3];                             //        // +0x2ed
#endif

    GDI_TEB_BATCH   GdiTebBatch;                             // +0x1d4 // +0x2f0
    CLIENT_ID       RealClientId;                            // +0x6b4 // +0x7d8
    PVOID           GdiCachedProcessHandle;                  // +0x6bc // +0x7e8
    DWORD           GdiClientPID;                            // +0x6c0 // +0x7f0
    DWORD           GdiClientTID;                            // +0x6c4 // +0x7f4
    PVOID           GdiThreadLocalInfo;                      // +0x6c8 // +0x7f8
    DWORD_PTR       Win32ClientInfo[0x3E];//[62];            // +0x6cc // +0x800
    PVOID           glDispatchTable[0xE9];//[233];           // +0x7c4 // +0x9f0
    DWORD_PTR       glReserved1[0x1D];//[29]                 // +0xb68 // +0x1138
    PVOID           glReserved2;                             // +0xbdc // +0x1220
    PVOID           glSectionInfo;                           // +0xbe0 // +0x1228
    PVOID           glSection;                               // +0xbe4 // +0x1230
    PVOID           glTable;                                 // +0xbe8 // +0x1238
    PVOID           glCurrentRC;                             // +0xbec // +0x1240
    PVOID           glContext;                               // +0xbf0 // +0x1248
    DWORD           LastStatusValue;                         // +0xbf4 // +0x1250
#if defined(_WIN64) // x64
    BYTE            Padding2[4];                                       // +0x1254
#endif
    UNICODE_STRING  StaticUnicodeString;                     // +0xbf8 // +0x1258
    WCHAR           StaticUnicodeBuffer[261];                // +0xc00 // +0x1268
#if defined(_WIN64) // x64
    BYTE            Padding3[6];                                       // +0x1472
#endif
    PVOID           DeallocationStack;                       // +0xe0c // +0x1478
    PVOID           TlsSlots[64];                            // +0xe10 // +0x1480
    LIST_ENTRY      TlsLinks;                                // +0xf10 // +0x1680
    PVOID           Vdm;                                     // +0xf18 // +0x1690
    PVOID           ReservedForNtRpc;                        // +0xf1c // +0x1698
    PVOID           DbgSsReserved[2];                        // +0xf20 // +0x16a0

    // To be continued
} TEB, *PTEB;


typedef struct _TEB32
{
    NT_TIB32        NtTib;                                 // +0x000
    DWORD           EnvironmentPointer;                    // +0x01c
    CLIENT_ID32     ClientId;                              // +0x020
    DWORD           ActiveRpcHandle;                       // +0x028
    DWORD           ThreadLocalStoragePointer;             // +0x02c
    DWORD           ProcessEnvironmentBlock;               // +0x030
    DWORD           LastErrorValue;                        // +0x034
    DWORD           CountOfOwnedCriticalSections;          // +0x038
    DWORD           CsrClientThread;                       // +0x03c
    DWORD           Win32ThreadInfo;                       // +0x040
    DWORD           User32Reserved[26];                    // +0x044
    DWORD           UserReserved[5];                       // +0x0ac
    DWORD           WOW32Reserved;                         // +0x0c0
    // To be continued
} TEB32, *PTEB32;

typedef struct _TEB64
{
    NT_TIB64        NtTib;                                 // +0x000
    QWORD           EnvironmentPointer;                    // +0x038
    CLIENT_ID64     ClientId;                              // +0x040
    QWORD           ActiveRpcHandle;                       // +0x050
    QWORD           ThreadLocalStoragePointer;             // +0x058
    QWORD           ProcessEnvironmentBlock;               // +0x060
    DWORD           LastErrorValue;                        // +0x068
    DWORD           CountOfOwnedCriticalSections;          // +0x06c
    QWORD           CsrClientThread;                       // +0x070
    QWORD           Win32ThreadInfo;                       // +0x078
    DWORD           User32Reserved[26];                    // +0x080
    DWORD           UserReserved[5];                       // +0x0e8
    QWORD           WOW32Reserved;                         // +0x100
    // To be continued
} TEB64, *PTEB64;


/* Official
typedef struct _PROCESS_BASIC_INFORMATION {
PVOID Reserved1;
PPEB PebBaseAddress;
PVOID Reserved2[2];
ULONG_PTR UniqueProcessId;
PVOID Reserved3;
} PROCESS_BASIC_INFORMATION;
*/

typedef struct _PROCESS_BASIC_INFORMATION
{
    ULONG_PTR   ExitStatus;
    PPEB        PebBaseAddress;
    ULONG_PTR   AffinityMask;
    ULONG_PTR   BasePriority;
    ULONG_PTR   UniqueProcessId;
    ULONG_PTR   InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFORMATION, *PPROCESS_BASIC_INFORMATION;

typedef struct _PROCESS_BASIC_INFORMATION32
{
    ULONG       ExitStatus;
    DWORD       PebBaseAddress;
    ULONG       AffinityMask;
    ULONG       BasePriority;
    ULONG       UniqueProcessId;
    ULONG       InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFORMATION32, *PPROCESS_BASIC_INFORMATION32;

typedef struct _PROCESS_BASIC_INFORMATION64
{
    ULONGLONG   ExitStatus;
    QWORD       PebBaseAddress;
    ULONGLONG   AffinityMask;
    ULONGLONG   BasePriority;
    ULONGLONG   UniqueProcessId;
    ULONGLONG   InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFORMATION64, *PPROCESS_BASIC_INFORMATION64;



#pragma warning( pop )


#ifdef UNICODE
#define GetRemoteModuleHandle  GetRemoteModuleHandleW
#else
#define GetRemoteModuleHandle  GetRemoteModuleHandleA
#endif // !UNICODE


HMODULE WINAPI
GetRemoteModuleHandleA(
    _In_opt_ HANDLE hProcess,
    _In_opt_ LPCSTR lpModuleName,
    _In_  DWORD dwBitnessFilterFlag
);

HMODULE WINAPI
GetRemoteModuleHandleW(
    _In_opt_ HANDLE hProcess,
    _In_opt_ LPCWSTR lpModuleName,
    _In_  DWORD dwBitnessFilterFlag
);

#ifndef _WIN64

#ifdef UNICODE
#define Wow64GetNativeModuleHandle  Wow64GetNativeModuleHandleW
#else
#define Wow64GetNativeModuleHandle  Wow64GetNativeModuleHandleA
#endif // !UNICODE


QWORD
WINAPI
Wow64GetNativeModuleHandleA(
    _In_opt_ LPCSTR lpModuleName
);

QWORD
WINAPI
Wow64GetNativeModuleHandleW(
    _In_opt_ LPCWSTR lpModuleName
);

QWORD
Wow64GetNativeProcAddress(
    _In_ QWORD   hModule,
    _In_ LPCSTR  lpProcName
);

#endif //!_WIN64

FARPROC WINAPI
GetProcAddressFromProcess(
    _In_opt_ HANDLE hProcess,
    _In_ HMODULE hModule,
    _In_ LPCSTR  lpProcName
);

BOOL WINAPI
IsWindows64Bit(
    _Out_ PBOOL  Windows64Bit
);

#endif//_WINDOWS_EX_H_
